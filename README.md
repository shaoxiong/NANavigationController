#NANavigationController

一个自定义的导航控制器，当在某个`ViewController`中的`NavigationBar`设置透明的时候，既可以通过新包装一个`NavigationController`来完成连续的动画切换。有关想法参考了[JTNavigationController](https://github.com/JNTian/JTNavigationController)的方式。

不同的是，当Push时，当前页面与下个页面的NavigationBar隐藏状态不同的时候才会包装`NavigationController`进行切换，若两者隐藏状态相同（均隐藏，均不隐藏）的时候，则使用原生动画进行过渡。

此外`NANavigationController`支持使用StoryBoard。

####如何使用：

 1. 复制工程里的相关NA开头的文件以及`UIViewController+NANav`至您的项目中。
 2. 在StoryBoard中将`NavigationController`设置成为为`NANavigationController`
 3. 在`ViewController`之间的Segue之间选择`NANavSegue`方式进行Push。
 4. 所有需要隐藏`NavigationBar`的`ViewController`需要添加`UIViewController+NANav.h`文件至头文件并设置隐藏状态，如果不需要隐藏可以不设置。


----------


###PS：本项目目前还没有经过完全测试，请谨慎使用。