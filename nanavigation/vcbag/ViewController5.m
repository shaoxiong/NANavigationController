//
//  ViewController5.m
//  nanavigation
//
//  Created by zhangyue on 16/4/7.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "ViewController5.h"

@interface ViewController5 ()

@end

@implementation ViewController5

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (void)setTransparentParam{
    [self setTransparentNavigationBarBool:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"vc5";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"popToVc2" style:UIBarButtonItemStylePlain target:self action:@selector(popToVC)];
}

- (void)popToVC{
    UIViewController * vc = [[self.naNavigationController viewControllerArray] objectAtIndex:1];
    [self.naNavigationController popToViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)seguePush:(id)sender {
    [self performSegueWithIdentifier:@"viewcontroller6Push" sender:self];
}


- (void)performBlock:(void(^)())block afterDelay:(NSTimeInterval)delay {
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}
@end
