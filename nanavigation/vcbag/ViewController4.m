//
//  ViewController4.m
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "ViewController4.h"

@implementation ViewController4

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (void)setTransparentParam{
    [self setTransparentNavigationBarBool:NO];
}


- (void)viewDidLoad{
    [super viewDidLoad];
}
- (IBAction)codePush:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController5"];
    [self.naNavigationController pushViewController:vc animated:YES];
}
@end
