//
//  ViewController6.m
//  nanavigation
//
//  Created by zhangyue on 16/4/7.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "ViewController6.h"

@interface ViewController6 ()

@end

@implementation ViewController6

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (void)setTransparentParam{
    [self setTransparentNavigationBarBool:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"vc6";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)popToRoot:(id)sender {
    [self.naNavigationController popToRootViewControllerAnimated:YES];
}


@end
