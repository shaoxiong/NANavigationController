//
//  ViewController3.m
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "ViewController3.h"

@implementation ViewController3
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        [self setTransparentParam];
    }
    return self;
}

- (void)setTransparentParam{
    [self setTransparentNavigationBarBool:YES];
}
- (void)viewDidLoad{
    [super viewDidLoad];
}
@end
