//
//  ViewController2.m
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "ViewController2.h"

@implementation ViewController2
- (void)viewDidLoad{
    [super viewDidLoad];
}
- (IBAction)toRoot:(id)sender {
    [self.naNavigationController popToRootViewControllerAnimated:YES];
}
@end
