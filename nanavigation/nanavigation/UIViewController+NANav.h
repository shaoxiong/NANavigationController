//
//  UIViewController+NANav.h
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NANavigationController.h"
@interface UIViewController (NANav)
/**
 * navigationBar是否透明
 */
@property (assign ,nonatomic) BOOL transparentNavigationBarBool;
/**
 * 主nanavigation
 */
@property (assign ,nonatomic) NANavigationController * naNavigationController;
/**
 * 用于自定义返回按钮点击功能
 */
@property (copy, nonatomic) void (^didBackClicked) (void);
@end
