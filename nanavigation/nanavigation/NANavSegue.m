//
//  NANavSegue.m
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "NANavSegue.h"
#import "UIViewController+NANav.h"
@implementation NANavSegue

- (void)perform {
    UIViewController * svc = (UIViewController *)self.sourceViewController;
    UIViewController * dvc = (UIViewController *)self.destinationViewController;
    [svc.naNavigationController pushViewController:dvc animated:YES];
}

- (void)setDestinationContainmentContext:(int)arg1{
    
}
@end
