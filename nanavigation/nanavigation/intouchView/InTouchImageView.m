//
//  InTouchImageView.m
//  nanavigation
//
//  Created by zhangyue on 16/4/7.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "InTouchImageView.h"

@implementation InTouchImageView{
    BOOL usingGr;
}


- (void)setOnTouchBlock:(void(^)(void))didClicked{
    self.userInteractionEnabled = YES;
    usingGr = NO;
    self.didClicked = didClicked;
}

- (void)setGestureRecognizersBlock:(void(^)(void))didClicked{
    self.userInteractionEnabled = YES;
    usingGr = YES;
    self.didClicked = didClicked;
    if (didClicked) {
        UITapGestureRecognizer * tapGr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickedWithGr)];
        [self addGestureRecognizer:tapGr];
    }
}

- (void)didClickedWithGr{
    if (self.didClicked && usingGr) {
        self.didClicked();
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [UIView animateWithDuration:.08f delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.alpha = .2f;
    } completion:^(BOOL finished) {
    }];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [UIView animateWithDuration:.08f delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.alpha = 1.f;
    } completion:^(BOOL finished) {
        if (self.didClicked && !usingGr) {
            self.didClicked();
        }
    }];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [UIView animateWithDuration:.08f delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.alpha = 1.f;
    } completion:^(BOOL finished) {
        if (self.didClicked && !usingGr) {
            self.didClicked();
        }
    }];
}
@end
