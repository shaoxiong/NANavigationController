//
//  InTouchImageView.h
//  nanavigation
//
//  Created by zhangyue on 16/4/7.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InTouchImageView : UIImageView
@property (copy, nonatomic) void (^didClicked) (void);
- (void)setOnTouchBlock:(void(^)(void))didClicked;
- (void)setGestureRecognizersBlock:(void(^)(void))didClicked;
@end
