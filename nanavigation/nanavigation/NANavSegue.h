//
//  NANavSegue.h
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NANavSegue : UIStoryboardSegue
- (void)setDestinationContainmentContext:(int)arg1;
@end
