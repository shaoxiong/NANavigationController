//
//  NANavigationController.h
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface NANavigationController : UINavigationController
@property (assign ,nonatomic) BOOL showBackTitle;
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated;
- (NSArray*)viewControllerArray;
- (void)removeViewController:(UIViewController*)vc;
@end
