//
//  NASubNavigationController.h
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NANavigationController.h"
@protocol NASubNavBackPanEnbaleDelegate <NSObject>
@required
- (void)switchMainNavBackPanEnable:(BOOL)enable;
- (void)didSubNavShowViewController:(UIViewController*)viewController;
@end
@interface NASubNavigationController : UINavigationController
@property (nonatomic ,assign) id <NASubNavBackPanEnbaleDelegate> na_delegate;
@end
