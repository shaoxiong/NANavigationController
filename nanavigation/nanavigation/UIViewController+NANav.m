//
//  UIViewController+NANav.m
//  nanavigation
//
//  Created by zhangyue on 16/4/6.
//  Copyright © 2016年 zeroflow. All rights reserved.
//

#import "UIViewController+NANav.h"
#import <objc/runtime.h>
@implementation UIViewController (NANav)
- (void)setTransparentNavigationBarBool:(BOOL)transparentNavigationBarBool{
    objc_setAssociatedObject(self, @selector(transparentNavigationBarBool), @(transparentNavigationBarBool), OBJC_ASSOCIATION_RETAIN);
}
- (BOOL)transparentNavigationBarBool {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}
- (void)setNaNavigationController:(NANavigationController *)naNavigationController{
    objc_setAssociatedObject(self, @selector(naNavigationController), naNavigationController, OBJC_ASSOCIATION_RETAIN);
}
- (NANavigationController *)naNavigationController{
    return objc_getAssociatedObject(self, _cmd);
}
- (void)setDidBackClicked:(void (^)(void))didBackClicked{
    objc_setAssociatedObject(self, @selector(didBackClicked), didBackClicked, OBJC_ASSOCIATION_RETAIN);
}
- (void (^)(void))didBackClicked{
    return objc_getAssociatedObject(self, _cmd);
}
@end
